import React, {useState, useEffect} from 'react'
 import Footer from './components/Footer'
import Header from './components/Header'
import Cart from './components/Cart'
import Products from './components/Products'
import Container from './elements/Container'
//import Product from './components/Product'


const App = () => {
  const [dataProducts, setDataProducts] = useState([])
//const [showCart, setShowCart] = useState(false)

  const getDataProducts = async() =>{
    const data = await fetch('https://5d8cdb5a443e3400143b4bea.mockapi.io/corebizchile/products')
    const productsElement = await data.json()
    // console.log(productsElement);
    setDataProducts(productsElement)
  }
  
  useEffect(() => {
  console.log('funciona');
   getDataProducts()
  }, [])


const addProductsToCart =( idProductAdd,productAdd, price, )=>{
  if (dataProducts.length ===0) {
setDataProducts([
 { id:idProductAdd, product:productAdd , price:price, quantity:1}
])}else{

  const newCart = [...dataProducts];
  const checkProductInCart = newCart.filter((productInCart)=>{
    return productInCart.id === idProductAdd
  }).length > 0;
  if(checkProductInCart){
    newCart.forEach((productInCart, index)=>{
      if(productInCart.id === idProductAdd){
        const cant = newCart[index].quantity;
        newCart[index]={
          id:idProductAdd, product:productAdd , price:price, quantity:cant +1
        }
      }
    })
  }else{
newCart.push({
  id:idProductAdd, product:productAdd , price:price, quantity:1
})
}
setDataProducts(newCart)
}

}



  return (
  
      <Container>
      
      <Header/>
    
    
      <Products  
      dataProducts={dataProducts}
      addProductsToCart ={addProductsToCart }
      />
     
      <aside>
       <Cart  dataProducts={dataProducts}  addProductsToCart ={addProductsToCart }/> 
      </aside>
       <Footer/>


      </Container>
  
  )
}

export default App
