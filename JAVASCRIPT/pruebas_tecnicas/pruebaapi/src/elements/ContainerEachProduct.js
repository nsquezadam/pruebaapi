import styled from 'styled-components'

const ContainerProduct = styled.div`

width: 10rem;
height:13rem;
display:flex;
flex-direction:column;
margin-left:6vw;



`
const ContainerInfo = styled.div`
background: #D2D1D1;
font-size:0.525rem;
height: 7rem;
text-align:center;



`
const ContainerImage = styled.div`
width:10rem;
height:6rem;
`
const Image = styled.img`
width:100%;
height:100%;

`
const Boton = styled.button`
width:6rem;
height:1.7rem;
border-radius:7px;
background:#000000;
color: #ffffff;
font-size:0.730rem;
border-style:none;
margin-top:0.3vh;
box-shadow:0px 2px 5px #555353;
`

export {
  ContainerProduct,
  ContainerInfo,
  ContainerImage,
  Image,
  Boton,
}