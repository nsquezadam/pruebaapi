import styled from 'styled-components'
import banner from  '../statics/banner.jpg'

const  ContainerHeader = styled.div `
width:100vw;
height:40vh;
background:url(${banner});
background-size:cover;
position:fixed;
z-index:1;
left:0;
right:0;
top:0;
`

const SearchBar = styled.div`

background: #fff;
width:100vw;
height: 5vh;
position:fixed;
z-index:2;
left:0;
right:0;
top:0;
display: flex;
flex-direction:row;
justify-content:center;


`
const ContainerLogo = styled.div`
display: flex;
width:9vw;
margin-left:5vw;
height:4vh;
align-items:center;
justify-content:center;
margin-right:10vw;


`

const Logo = styled.img`
width:100%;
height:100%;
padding-top:0.9vh;


`

const Input = styled.input`
border-style:none;
border-bottom:1px solid #D2D1D1;
width:35vw;
align-self:center;
padding-top:1.5vh;
margin-right:1.5vw;
`

const ContainerAccount = styled.div`
display: flex;
flex-direction:row;
align-content:center;
width:7vw;
padding-top:1vh;




`

const TextAcount = styled.p`
color:#676464;
font-size:0.520rem;
padding-top:1vh;
margin-left:0.5vw;
`
const ContainerCart= styled.div`
display: flex;
width:10vw;
flex-direction:row;
padding-top:1vh;

`
const ContainerCount = styled.div`
background: red;
width:12px;
height:12px;
border-radius:6px;
padding:1px;
margin-left:0.2vw;
margin-top:0.8vh;
`

const TextCount = styled.div`
color:#ffffff;
font-size:0.430rem;
text-align:center;

`

export  {
  ContainerHeader,
  SearchBar,
  ContainerLogo,
  Logo,
  Input,
  ContainerAccount,
  TextAcount,
  ContainerCart,
  ContainerCount,
  TextCount

};