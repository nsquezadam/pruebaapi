import React from 'react'
import Product from './Product'
import {
  Container,
   SliderProducts,
  TitleAllProducts,
  } from '../elements/ContainerAllProducts'
// import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
// import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';


const Products = ({dataProducts,addProductsToCart }) => {
  return (
    <Container>
      <TitleAllProducts>Mais Vendidos</TitleAllProducts>
<SliderProducts>
<Product dataProducts={dataProducts} addProductsToCart={addProductsToCart }/>
{/* <ButtonLeft><img src={ArrowBackIosIcon} alt="back"/></ButtonLeft>
<ButtonRight><img src={ArrowForwardIosIcon } alt="forward"/></ButtonRight> */}
</SliderProducts>
      
    </Container>
  )
}

export default Products
