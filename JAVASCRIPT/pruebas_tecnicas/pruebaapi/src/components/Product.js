import React from 'react'
import {
  ContainerProduct,
  ContainerInfo,
  ContainerImage,
  Image,
  Boton
} from '../elements/ContainerEachProduct'
import star from '../statics/star.png'
import starAlt from '../statics/star_alt.png'

const Product = ({dataProducts,addProductsToCart }) => {


  return (
    <>
  
{dataProducts.map((product)=>{
  return (
    <ContainerProduct key={product.id}>
      <ContainerImage >
        <Image  src={`${product.img}`} alt={product.id}/>
      </ContainerImage>
      <ContainerInfo>
        <h3>{product.product}</h3>
        <div>
          <img src={star} alt="star"/>
          <img src={starAlt} alt="star"/>
          <img src={starAlt} alt="star"/>
          <img src={starAlt} alt="star"/>
          <img src={starAlt} alt="star"/>
        </div>
        <p>{product.price}</p>
        <p>{product.sku}</p>
        <Boton onClick={()=>addProductsToCart(product.id, product.product)}>Comprar</Boton>
      </ContainerInfo>
  </ContainerProduct>
)

})

}


</>
  )
}

export default Product
