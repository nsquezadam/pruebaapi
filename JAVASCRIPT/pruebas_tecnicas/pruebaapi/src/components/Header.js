import React from 'react'
import { 
  ContainerHeader,
  SearchBar,
   ContainerLogo, 
   Logo,
   Input,
   ContainerAccount,
   TextAcount,
   ContainerCart,
   ContainerCount,
   TextCount
  } from '../elements/ContainerHead'
import account from '../statics/account.png'
import shoppingCart from '../statics/shopping-cart.png'
import logo from '../statics/logo.png'

const Header = () => {
  return (
    <ContainerHeader>
      <SearchBar>
        <ContainerLogo>
          <Logo src={logo} alt=""/>
        </ContainerLogo>
        <form action="">
          <Input 
          type="text"
          placeholder="O que está procurando?" /> 
        </form>
        <ContainerAccount>
        <span><img src={account} alt="account"/></span>
        <TextAcount>Minha Conta</TextAcount>
        </ContainerAccount>
        <ContainerCart>
          <span><img src={shoppingCart} alt="cart"/></span>
          <ContainerCount>
            <TextCount>3</TextCount>
          </ContainerCount>
        </ContainerCart>
      </SearchBar>
      
    </ContainerHeader>
  )
}

export default Header
